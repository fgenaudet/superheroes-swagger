package ch.fgenaudet.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.sql.rowset.serial.SerialClob;
import javax.sql.rowset.serial.SerialException;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import ch.fgenaudet.bean.Power;
import ch.fgenaudet.bean.Superhero;
import ch.fgenaudet.repository.PowerRepository;
import ch.fgenaudet.repository.SuperheroesRepository;

import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/superheroes")
public class AnnotatedSuperHeroesController {
	@Autowired
	private SuperheroesRepository superheroesRepository;
	@Autowired
	private PowerRepository powerRepository;

	@RequestMapping(method = RequestMethod.GET, value = "/")
	@ApiOperation(value = "Get all superheroes")
	public List<Superhero> listSuperheroes() {
		return (List<Superhero>) superheroesRepository.findAll();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	@ApiOperation(httpMethod = "GET", value = "Find a superhero")
	public Superhero findASuperhero(@ApiParam(value = "ID of the superhero", required = true) @PathVariable Long id) {
		return superheroesRepository.findOne(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/")
	@ApiOperation(value = "Create a superhero and his powers")
	public Long createASuperhero(
			@RequestBody @ApiParam(name = "Superhero to persist", required = true) Superhero superhero) {
		
		saveNewPowers(superhero);
		Superhero savedHero = superheroesRepository.save(superhero);

		return savedHero.getId();
	}

	private void saveNewPowers(Superhero superhero) {
		for (Power power : superhero.getPowers()) {
			if (power.getId() == null) {
				Power savedPower = powerRepository.save(power);
				power.setId(savedPower.getId());
			}
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/avatar/{id}")
	@ApiOperation(value = "Upload a superhero avatar")
	public Long changeSuperheroAvatar(@PathVariable @ApiParam(value = "ID of the superhero", required = true) Long id,
			@RequestPart("avatar") @ApiParam(name = "avatar", required = true) MultipartFile avatar)
			throws IOException, SerialException, SQLException {
		Superhero hero = superheroesRepository.findOne(id);

		byte[] avatarInBase64 = Base64.encodeBase64(avatar.getBytes());
		hero.setAvatar(new SerialClob(new String(avatarInBase64).toCharArray()));

		superheroesRepository.save(hero);

		return hero.getId();
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	@ApiOperation(value = "Delete a superhero avatar")
	public void deleteASuperhero(@PathVariable Long id) {
		superheroesRepository.delete(id);
	}
}
