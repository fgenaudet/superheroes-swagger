package ch.fgenaudet.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.sql.rowset.serial.SerialClob;
import javax.sql.rowset.serial.SerialException;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import ch.fgenaudet.bean.Superhero;
import ch.fgenaudet.repository.SuperheroesRepository;

@RestController
@RequestMapping("/heroes")
public class SuperHeroesController {
	@Autowired
	private SuperheroesRepository superheroesRepository;

	@RequestMapping(method = RequestMethod.GET, value = "/")
	public List<Superhero> listSuperheroes() {
		return (List<Superhero>) superheroesRepository.findAll();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public Superhero findASuperhero(@PathVariable Long id) {
		return superheroesRepository.findOne(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/")
	public Long createASuperhero(@RequestBody Superhero superhero) {
		Superhero savedHero = superheroesRepository.save(superhero);

		return savedHero.getId();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/avatar/{id}")
	public Long changeSuperheroAvatar(@PathVariable Long id, @RequestPart("file") MultipartFile avatar)
			throws IOException, SerialException, SQLException {
		Superhero hero = superheroesRepository.findOne(id);

		byte[] avatarInBase64 = Base64.encodeBase64(avatar.getBytes());
		hero.setAvatar(new SerialClob(new String(avatarInBase64).toCharArray()));

		return hero.getId();
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void deleteASuperhero(@PathVariable Long id) {
		superheroesRepository.delete(id);
	}
}
