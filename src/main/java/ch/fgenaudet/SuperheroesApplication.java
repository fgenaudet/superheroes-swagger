package ch.fgenaudet;

import java.sql.SQLException;

import org.h2.tools.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.mangofactory.swagger.plugin.EnableSwagger;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@EnableSwagger
public class SuperheroesApplication {

    public static void main(String[] args) throws SQLException {
    	Server.createWebServer("-webPort", "9092", "-tcpAllowOthers").start();
        SpringApplication.run(SuperheroesApplication.class, args);
    }
}
