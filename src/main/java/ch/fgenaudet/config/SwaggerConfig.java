package ch.fgenaudet.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mangofactory.swagger.configuration.SpringSwaggerConfig;
import com.mangofactory.swagger.models.dto.ApiInfo;
import com.mangofactory.swagger.plugin.EnableSwagger;
import com.mangofactory.swagger.plugin.SwaggerSpringMvcPlugin;

@Configuration
@EnableSwagger
public class SwaggerConfig {
	@Autowired
	private SpringSwaggerConfig springSwaggerConfig;

	@Bean
	public SwaggerSpringMvcPlugin customImplementation() {
		return new SwaggerSpringMvcPlugin(springSwaggerConfig)
			.apiInfo(apiInfo())
			.includePatterns("/heroes/.*", "/superheroes/.*")
			.apiVersion("1.0.0");
	}

	private ApiInfo apiInfo() {
		ApiInfo apiInfo = new ApiInfo("Superheroes", "API for superheroes", "API terms of service",
				"florian.genaudet@gmail.com", "Copyright FGenaudet", "http://florian-genaudet.fr");
		return apiInfo;
	}

}