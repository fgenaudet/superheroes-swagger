package ch.fgenaudet.repository;

import org.springframework.data.repository.CrudRepository;

import ch.fgenaudet.bean.Power;

public interface PowerRepository extends CrudRepository<Power, Long>{

}
