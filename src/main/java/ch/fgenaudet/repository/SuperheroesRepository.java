package ch.fgenaudet.repository;

import org.springframework.data.repository.CrudRepository;

import ch.fgenaudet.bean.Superhero;

public interface SuperheroesRepository extends CrudRepository<Superhero, Long>{

}
