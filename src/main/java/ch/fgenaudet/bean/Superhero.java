package ch.fgenaudet.bean;

import java.io.InputStream;
import java.io.StringWriter;
import java.sql.Clob;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@Entity
@ApiModel(description="Represents a Superhero")
public class Superhero {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@ApiModelProperty(dataType="Long", notes="The internal id of a superhero")
	private Long id;
	
	private String name;
	private String realName;
	
	@JsonIgnore
	private Clob avatar;
	
	@OneToMany(orphanRemoval = true)
	@JoinColumn(name="SUPERHERO_ID", referencedColumnName="ID")
	private List<Power> powers;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public List<Power> getPowers() {
		return powers;
	}

	public void setPowers(List<Power> powers) {
		this.powers = powers;
	}

	public Clob getAvatar() {
		return avatar;
	}

	public void setAvatar(Clob avatar) {
		this.avatar = avatar;
	}
	
	public String getAvatarAsString() {
		if (this.avatar != null) {
			try {
				InputStream in = avatar.getAsciiStream();
				StringWriter w = new StringWriter();
				IOUtils.copy(in, w);
				
				return w.toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
